// Find documents that have an age of less than 50.
db.users.find({age: {$lt: 50}})
db.users.find({age: {$exists: true, $lt: 50}})

// Find documents that have an age greater than 50.
db.users.find({age: {$gt: 50}})

// Find documents not equal to 82
db.users.find(
{
    age: {$ne: 82}
}
)

// Find documents that has names hawking and doe
db.users.find({lastName: {$in: ['Armstrong', 'Doe']}})

// Find documentss that has a course of PHP
db.users.find({courses: {$in: ['PHP']}})

// Logical Query Operators
db.users.find({
	$or: [
	{firstName: "Neil"},
	{age: {$gt: 30}
	]
})

// neil or age greater than 30
db.users.find({
	$or: [
	{firstName: "Neil"},
	{age: {$gt: 30}}
	]
})

db.users.find({
	$and: [
	{age: {$ne: 72}},
	{age: {$ne: 72}}
	]
})

// Projection - returns id, firstname, lastname and contact
db.users.find(
{
	firstName: "Jane"
}, 
{
		firstName: 1,
		lastName: 1,
		contact: 1
}
)

db.users.find(
{
	firstName: "Neil"
}, 
{
	department:  0,
	contact: 0
}
)

db.users.find(
{
	firstName: "Neil"
}, 
{
	email: 0
}
)

db.users.find(
{
	firstName: {$regex: "n", $options: 'i'}
}
)


